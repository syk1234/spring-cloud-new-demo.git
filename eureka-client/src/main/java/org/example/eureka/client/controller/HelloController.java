package org.example.eureka.client.controller;

import com.netflix.discovery.EurekaClient;
import lombok.extern.slf4j.Slf4j;
import org.example.eureka.client.service.HelloService;
import org.example.eureka.client.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

/**
 * 代码描述<p>
 *
 * @author xuker
 * @since 2021/4/27 上午10:34
 */
@RestController
@Slf4j
public class HelloController {

    @Autowired
    private EurekaClient discoveryClient;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String hello() {
        return helloService.hello();
    }

    @RequestMapping(value = "/hello1", method = RequestMethod.GET)
    public String hello(@RequestParam("name") String name) {
        return helloService.hello(name);
    }

    @RequestMapping(value = "/hello2", method = RequestMethod.GET)
    public User hello(@RequestParam("name") String name, @RequestHeader("age") Integer age) {
        return helloService.hello(name, age);
    }

    @RequestMapping(value = "/hello3", method = RequestMethod.POST)
    public String hello(@RequestBody User user) {
        return helloService.hello(user);
    }

    @RequestMapping(value = "helloTest", method = RequestMethod.GET)
    public String helloTest() {
        log.info("hello-client");
        discoveryClient.getApplications().getAppsHashCode();
        return "hello client";
    }

    @RequestMapping(value = "helloHystrix", method = RequestMethod.GET)
    public String helloHystrix() {
        return helloService.helloHystrix();
    }

    @RequestMapping(value = "call", method = RequestMethod.GET)
    public String call() {
        return restTemplate.getForEntity("http://CLIENT1/hello", String.class).getBody();
    }

    @RequestMapping(value = "/config", method = RequestMethod.GET)
    public String config() {
        return helloService.config();
    }
}
