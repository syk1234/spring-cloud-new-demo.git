package org.example.eureka.client.vo;

import lombok.Data;

/**
 * 代码描述<p>
 *
 * @author xuker
 * @since 2021/5/28 下午2:36
 */
@Data
public class User {

  private String name;
  private Integer age;
}
