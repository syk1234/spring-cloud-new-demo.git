package org.example.eureka.client.service;

import org.example.eureka.client.vo.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * 代码描述<p>
 *
 * @author xuker
 * @since 2021/5/28 下午2:43
 */
@Slf4j
@Service
public class HelloServiceImpl implements HelloService {
  @Autowired
  private RestTemplate restTemplate;

  @Override
  public String hello() {
    log.info("hello");
    return "hello";
  }

  @Override
  public String hello(String name) {
    log.info("hello:{}", name);
    return "hello:" + name;
  }

  @Override
  public User hello(String name, Integer age) {
    log.info("hello:{},age={}", name, age);
    User user = new User();
    user.setName(name);
    user.setAge(age);
    return user;
  }

  @Override
  public String hello(User user) {
    log.info("hello user,hello:{},age={}", user.getName(), user.getAge());
    return "hello:" + user.getName() + ",age=" + user.getAge();
  }

  @Override
  public String helloHystrix() {
    int i = RandomUtils.nextInt(30000);
    log.info("sleep {} seconds", i);
    try {
      Thread.sleep(RandomUtils.nextInt(i));
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return "hello hystrix";
  }

  @Override
  public String config() {
    return restTemplate.getForEntity("http://CONFIG-CLIENT/from", String.class).getBody();
  }
}
