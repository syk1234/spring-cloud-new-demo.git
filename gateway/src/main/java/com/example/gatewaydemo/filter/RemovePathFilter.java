package com.example.gatewaydemo.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.addOriginalRequestUrl;
@Slf4j
public class RemovePathFilter implements GlobalFilter, Ordered {

    private static final String PREFIX = "/gateway/";

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("删除api前缀");
        ServerHttpRequest req = exchange.getRequest();

        addOriginalRequestUrl(exchange, req.getURI());
        String path = req.getURI().getRawPath();
        if (path.contains(PREFIX)) {
            String newPath = path.replace(PREFIX, "/");
            req = req.mutate().path(newPath).build();
            exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, req.getURI());
        }
        return chain.filter(exchange.mutate().request(req).build());
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
