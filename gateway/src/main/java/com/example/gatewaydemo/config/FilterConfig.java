package com.example.gatewaydemo.config;

import com.example.gatewaydemo.feign.TestFeignClient;
import com.example.gatewaydemo.filter.*;
import org.springframework.cloud.gateway.filter.factory.rewrite.MessageBodyDecoder;
import org.springframework.cloud.gateway.filter.factory.rewrite.MessageBodyEncoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.ServerCodecConfigurer;

import java.util.Set;

@Configuration(proxyBeanMethods = false)
public class FilterConfig {
    @Bean
    public RemovePathFilter removePathFilter() {
        return new RemovePathFilter();
    }

    //    @Bean
    public ExceptionFilter exceptionFilter() {
        return new ExceptionFilter();
    }

    @Bean
    public FirstFilter firstFilter() {
        return new FirstFilter();
    }
    @Bean
    public FeignTestFilter feignTestFilter(TestFeignClient testFeignClient) {
        return new FeignTestFilter(testFeignClient);
    }

    @Bean
    public SecondFilter secondFilter() {
        return new SecondFilter();
    }

    @Bean
    public ModifyRequestBodyFilter modifyRequestBodyFilter() {
        return new ModifyRequestBodyFilter();
    }

    @Bean
    public ModifyResponseBodyFilter modifyResponseBodyFilter(
            ServerCodecConfigurer codecConfigurer, Set<MessageBodyDecoder> bodyDecoders,
            Set<MessageBodyEncoder> bodyEncoders) {
        return new ModifyResponseBodyFilter(bodyDecoders, bodyEncoders, codecConfigurer.getReaders());
    }
}
